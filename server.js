// builtin modules
var http = require('http')

// external modules
var static = require('node-static')

// global objects
var fileServer = new static.Server('./public')
var httpServer = http.createServer()

httpServer.on('request', function (req, res) {
    fileServer.serve(req, res)
}).listen(8888)

console.log('Backend started')